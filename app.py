from flask import Flask, request, render_template, redirect, url_for
from redis import Redis, RedisError
import os
import socket
import pymysql
# Connect to Redis
redis = Redis(host="redis", db=0, socket_connect_timeout=2, socket_timeout=2)

app = Flask(__name__)

def openConnection():
    global conn
    conn = pymysql.connect(host='192.168.99.100', user='root', password='abc123', db='playlist' )


@app.route('/cercaPlaylist',  methods=['GET', 'POST'])
def querySelect():
    if request.method == 'POST':
        cercaPlaylist=request.form['Invia']
        openConnection()

        with conn.cursor() as cur:
            sql = "select playlist.CANZONE.* from (playlist.COMPOSIZIONE join playlist.UTENTE ON COMPOSIZIONE.utente = UTENTE.id_utente) join playlist.CANZONE on CANZONE.id = COMPOSIZIONE.canzone where UTENTE.id_utente = %s"
            #cur.execute("SELECT * from playlist.UTENTE") -> funziona
            cur.execute(sql, cercaPlaylist)
            data = cur.fetchone()
            html="<h1>Tutte le canzoni di: " + cercaPlaylist + "</h1>"
            while data is not None:
                html += "<p>"+str(data)+"</p><br>"
                data = cur.fetchone()

            html += "<a href='javascript:history.back()'>torna al menu principale!</a>"
            cur.close()
            conn.close()

    return html


@app.route('/nuovaCanzone',  methods=['GET', 'POST'])
def queryInsert1():
    if request.method == 'POST':
        codice=request.form['codice']
        nome=request.form['nome']
        cantante=request.form['cantante']
        genere=request.form['genere']
        openConnection()
        with conn.cursor() as cur:
            sql = "INSERT INTO CANZONE (id,nome,cantante,genere)VALUES(%s,%s,%s,%s);"
            #cur.execute("SELECT * from playlist.UTENTE") -> funziona
            cur.execute(sql, (codice, nome, cantante, genere))
            conn.commit()
            cur.close()
            conn.close()
            html="<h1>"+"INSERIMENTO RIUSCITO"+"</h1> <br><a href='javascript:history.back()'>torna indietro</a>"

    else:
        html="<h1>"+"ERRORE"+"</h1> <br><a href='javascript:history.back()'>torna indietro</a>"


    return html

@app.route('/associa',  methods=['GET', 'POST'])
def queryInsert2():
    if request.method == 'POST':
        idc=request.form['idc']
        idu=request.form['idu']

        openConnection()
        with conn.cursor() as cur:
            sql = "INSERT INTO COMPOSIZIONE (utente,canzone)VALUES(%s,%s);"
            #cur.execute("SELECT * from playlist.UTENTE") -> funziona
            cur.execute(sql, (idu, idc))
            conn.commit()
            cur.close()
            conn.close()
            html="<h1>"+"INSERIMENTO RIUSCITO"+"</h1> <br><a href='javascript:history.back()'>torna indietro</a>"

    else:
        html="<h1>"+"ERRORE"+"</h1> <br><a href='javascript:history.back()'>torna indietro</a>"


    return html

@app.route('/rimuovi',  methods=['GET', 'POST'])
def queryInsert3():
    if request.method == 'POST':
        idc=request.form['idc']
        idu=request.form['idu']

        openConnection()
        with conn.cursor() as cur:
            sql = "DELETE FROM COMPOSIZIONE WHERE (utente = %s and canzone=%s);"
            #cur.execute("SELECT * from playlist.UTENTE") -> funziona
            cur.execute(sql, (idu, idc))
            conn.commit()
            cur.close()
            conn.close()
            html="<h1>"+"CANCELLAZIONE RIUSCITA"+"</h1> <br><a href='javascript:history.back()'>torna indietro</a>"

    else:
        html="<h1>"+"ERRORE"+"</h1> <br><a href='javascript:history.back()'>torna indietro</a>"


    return html



@app.route('/')
def index():
    return render_template('index.html')


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)
